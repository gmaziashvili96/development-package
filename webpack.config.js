const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
    devServer:{
        open:true,
        host:'10.10.24.76',
        port:1994,
        contentBase:path.resolve(__dirname,'dist'),
        publicPath:'/dist',
    },
    watch:true,
    entry:'./src/js/main.js',
    mode:'development',
    output:{
        path:path.resolve(__dirname,'dist'),
        filename:'app.js',
        publicPath:'/dist'
    },
    module:{
        rules:[
            {
                test:/\.js$/,
                use:'babel-loader'
            },
            {
                test: /\.scss$/,
                use: [
                  'style-loader',
                  MiniCssExtractPlugin.loader,
                  {
                    loader: 'css-loader',
                    options: { sourceMap: true }
                  },
                  {
                    loader: 'postcss-loader',
                    options: { 
                        sourceMap: true,
                        config:{path:'src/js/postcss.config.js'}}
                  },
                  {
                    loader: 'sass-loader',
                    options: { sourceMap: true }
                  }
                ]
            }
        ],
    },
    plugins:[
        new HtmlWebpackPlugin({
            template: './src/index.html',
            meta: {
                viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
                'theme-color': '#fff'
            }
        }),
        new MiniCssExtractPlugin({
            filename: "./css/style.css",
        })
    ],
}