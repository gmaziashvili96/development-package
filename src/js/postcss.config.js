module.exports = {
    plugins:[
        require('postcss-import'),
        require('autoprefixer'),
        require('css-mqpacker'),
        require('cssnano')({
            preset:[
                'default',{
                    discardComments:{
                        removeAll:true,
                    }
                }
            ]
        }),
    ]
}
// module.exports = {
//     parser: 'sugarss',
//     map: false,
//     plugins: {
//       'postcss-plugin': {}
//     }
//   }